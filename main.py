import pyrogram
from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardMarkup,InlineKeyboardButton
import os
import backend
from backend import SITE_URL
from doodstream import DoodStream

# bot
bot_token = os.environ.get("TOKEN", "") 
api_hash = os.environ.get("HASH", "")
api_id = os.environ.get("ID", "")
app = Client("my_bot",api_id=api_id, api_hash=api_hash,bot_token=bot_token)
manage = backend.Manager()


def tokenlogin(message,apitoken):

    status = backend.request_url(apitoken,"google.com")["status"]
    if status != "success":
        app.send_message(message.chat.id, "__❌ Invalid Token__", reply_to_message_id=message.id)
        return
    
    if manage.db.is_user_with_token(message.from_user.id):
        token = manage.db.fetch_token(message.from_user.id)[0]
        if apitoken == token:
            app.send_message(message.chat.id, f"__☑️ Already logged in with the same__\n**{apitoken}**", reply_to_message_id=message.id)
            return
        logouttext = f"__⭕ Logged out with token__\n**{token}**\n\nand "
        manage.update_token(message.from_user.id, apitoken)
    else:
        logouttext = ""
        manage.add_user_with_token(message.from_user.id, apitoken)

    app.send_message(message.chat.id, f"{logouttext}__✅ Logged in with token__\n**{apitoken}**", reply_to_message_id=message.id)


HELP_TEXT = "__**Basic Usage**\n\
🆕 /start - to check if i am alive\n\
🆘 /help - this message\n\
👨‍💻 /login username::password - to login to your account (to check statistics)\n\
📊 /stats - to check statistics of your account\n\
📱 /token apitoken - to login through APItoken (to short the link)\n\
✔️ /check - to check details of logins\n\
📹 /dood apikey - to login on Dood\n\
🎗️ /remote link - remote upload to Dood\n\
📠 /logout - to logout\n\n\
**Advanced Usage**\n\
🔼 /head text - to remove or set header\n\
🔽 /foot text - to remove or set footer\n\
💠 /onlylinks - to change behavior of texts\n\
📷 /noimage - to change behavior of images__"


# start
@app.on_message(filters.command(["start"]))
def send_welcome(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):    
    try:
        data = message.text.split('/start ')[1]
        tokenlogin(message,data.split("token_")[1])
    except:
        app.send_message(message.chat.id, f"__👋 Hi {message.from_user.mention}, i am a interface bot working for {SITE_URL}, just send me any link to shorten it for you.\nuse /help to know how to use me.__", reply_to_message_id=message.id,
        reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton("Help", callback_data="help")], [InlineKeyboardButton("🌐 WebSite", url="https://" + SITE_URL), InlineKeyboardButton("🕸️ Dood Stream",url="https://doodstream.com/")]]))


# help
@app.on_message(filters.command(["help"]))
def send_help(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    app.send_message(message.chat.id, HELP_TEXT, reply_to_message_id=message.id)


# call back
@app.on_callback_query()
def answer(client: pyrogram.client.Client, call: pyrogram.types.CallbackQuery):
    app.answer_callback_query(call.id)

    if call.data == "help":
        app.edit_message_text(call.message.chat.id, call.message.id, HELP_TEXT, reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton("Back", callback_data="back")]]))

    elif call.data == "back":
        app.edit_message_text(call.message.chat.id, call.message.id, f"__I am a interface bot working for {SITE_URL}, just send me any link to shorten it for you.\nuse /help to know how to use me.__",
        reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton("Help", callback_data="help")], [InlineKeyboardButton("🌐 WebSite", url="https://" + SITE_URL), InlineKeyboardButton("🕸️ Dood Stream",url="https://doodstream.com/")]]))


# login
@app.on_message(filters.command(["login"]))
def login(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    try:
        data = message.text.split("/login ")[1]
    except:
        app.send_message(message.chat.id, "__⚠️ Invalid Format\n/login username::password__", reply_to_message_id=message.id)
        return

    username = data.split("::")[0]
    password = data.split("::")[1]
    flag = manage.add_user_with_username(message.from_user.id,username,password)
    if flag == 1:
        app.send_message(message.chat.id, f"__✅ Logged in with account__\n**{username}:{password}**", reply_to_message_id=message.id)
    else:
        app.send_message(message.chat.id, f"__❌ Invalid Credentials__", reply_to_message_id=message.id)
    

# stats
@app.on_message(filters.command(["stats"]))
def stats(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    if manage.db.is_user_with_username(message.from_user.id):
        obj = manage.scrape(message.from_user.id)  
        table = "**Date\t\t\tViews\t\tEarnings\t\tCPM\t\tReferral**\n"
        text = f"**Total Views** : __{obj.total_view}__ \n**Total Earnings** : __{obj.total_earning}__\n**Total Referel Earnings** : __{obj.total_refferal_earning}__\n**Average CPM8** : __{obj.avg_cpm}__"
        obj.stats(f"{message.from_user.id}.jpg")
        app.send_photo(message.chat.id, f"{message.from_user.id}.jpg", text, reply_to_message_id=message.id)
        app.send_message(message.chat.id, f"{table}__{obj.table}__")
        os.remove(f"{message.from_user.id}.jpg")
    else:
        app.send_message(message.chat.id, "__🚫 You have not logged in with account, use /login__", reply_to_message_id=message.id)


# token
@app.on_message(filters.command(["token"]))
def token(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
   
    try:
        apitoken = message.text.split("/token ")[1]
    except:
        app.send_message(message.chat.id, "__⚠️ Invalid Format\n/token apitoken__", reply_to_message_id=message.id)
        return
    
    tokenlogin(message,apitoken)


# check
@app.on_message(filters.command(["check"]))
def check(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):

    if manage.db.is_user_with_token(message.from_user.id):
        token = manage.db.fetch_token(message.from_user.id)[0]
        data1 = f"__☑️ You are logged in with token__\n**{token}**"
    else:
        data1 = "__❎ You have not logged in with token__"

    if manage.db.is_user_with_username(message.from_user.id):
        username = manage.db.fetch_all(message.from_user.id)[4]
        password = manage.db.fetch_all(message.from_user.id)[5]
        data2 = f"__☑️ You are logged in with account__\n**{username}:{password}**"
    else:
        data2 = "__❎ You have not logged in with account__"

    if manage.db.is_user_with_dood_key(message.from_user.id):
        key = manage.db.fetch_dood_key(message.from_user.id)[0]
        data3 = f"__☑️ You are logged in with API Key__\n**{key}**"
    else:
        data3 = "__❎ You have not logged in with API Key__"

    app.send_message(message.chat.id, f"{data1}\n\n{data2}\n\n{data3}", reply_to_message_id=message.id)


# logout
@app.on_message(filters.command(["logout"]))
def logout(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):

    if manage.db.is_user_with_token(message.from_user.id):
        token = manage.db.fetch_token(message.from_user.id)[0]
        data1 = f"__⭕ You are logged out with token__\n**{token}**"
    else:
        data1 = "__❎ You have not logged in with token__"

    if manage.db.is_user_with_username(message.from_user.id):
        username = manage.db.fetch_all(message.from_user.id)[4]
        password = manage.db.fetch_all(message.from_user.id)[5]
        data2 = f"__⭕ You are logged out from account__\n**{username}:{password}**"
    else:
        data2 = "__❎ You have not logged in with account__"

    if manage.db.is_user_with_dood_key(message.from_user.id):
        key = manage.db.fetch_dood_key(message.from_user.id)[0]
        data3 = f"__⭕ You are logged out with API Key__\n**{key}**"
    else:
        data3 = "__❎ You have not logged in with API Key__"

    manage.delete_user(message.from_user.id)
    app.send_message(message.chat.id, f"{data1}\n\n{data2}\n\n{data3}", reply_to_message_id=message.id)
    

# dood
@app.on_message(filters.command(["dood"]))
def doodlogin(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):

    if not manage.db.is_user_with_token(message.from_user.id):
        app.send_message(message.chat.id, "__❗ First you have to log in with token,\nuse /token__", reply_to_message_id=message.id)
        return

    try:
        key = message.text.split("/dood ")[1]
    except:
        app.send_message(message.chat.id, "__⚠️ Invalid Format\n/dood apikey__", reply_to_message_id=message.id)
        return
    
    if not backend.checkkey(key):
        app.send_message(message.chat.id, "__❌  Invalid API KEY__", reply_to_message_id=message.id)
        return

    if manage.db.is_user_with_dood_key(message.from_user.id):
        apikey = manage.db.fetch_dood_key(message.from_user.id)[0]
        if apikey == key:
            app.send_message(message.chat.id, f"__☑️ Already logged in with the same key__\n**{apikey}**", reply_to_message_id=message.id)
            return
        logouttext = f"__⭕ Logged out with key__\n**{apikey}**\n\nand "
        manage.db.add_with_dood_key(message.from_user.id, key)
    else:
        logouttext = ""
        manage.db.add_with_dood_key(message.from_user.id, key)
    
    app.send_message(message.chat.id, f"{logouttext}__✅ Logged in with key__\n**{key}**", reply_to_message_id=message.id)


# remote
@app.on_message(filters.command(["remote"]))
def remote(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    try:
        link = message.text.split("/remote ")[1]
    except:
        app.send_message(message.chat.id, "__⚠️ Invalid Format\n/remote link__", reply_to_message_id=message.id)
        return

    if manage.db.is_user_with_dood_key(message.from_user.id) and manage.db.is_user_with_token(message.from_user.id):
        token = manage.db.fetch_token(message.from_user.id)[0]
        key = manage.db.fetch_dood_key(message.from_user.id)[0]
        d = DoodStream(key)
        try:
            info = d.remote_upload(link)
        except Exception as e:
            app.send_message(message.chat.id, text=f"__⛔ Error : {e}__", reply_to_message_id=message.id)
            return
        print(info)

        if info["msg"] == "OK":
            msg = app.send_message(chat_id=message.chat.id, text="__🔄 Processing__", reply_to_message_id=message.id)
            link = "https://doodstream.com/d/" + info["result"]["filecode"]
            resp = backend.request_url(token,link)
            if resp["status"] == "success": 
                surl = resp["shortenedUrl"]
                app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f'**{surl}**')
            else:
                app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f'__⛔ Failed as {resp["message"]}__') 
        else:
            app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f'__⛔ Failed as {info["msg"]}__') 

    else:
        app.send_message(message.chat.id, "__⁉️ You either not logged in with Token or Dood Key.\nuse /token  or  use /dood__", reply_to_message_id=message.id)


# head
@app.on_message(filters.command(["head"]))
def head(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    try:
        data = message.text.split("/head ")[1]
    except:
        if backend.getdata(message.from_user.id,"head") != "":
            backend.editdata(message.from_user.id,"head","")
            app.send_message(message.chat.id, "__🔼 Header is Removed__", reply_to_message_id=message.id)
        else:
            app.send_message(message.chat.id, "__🔺 Header is not Set,\nTo set Header use **/head text**__", reply_to_message_id=message.id)
        return

    backend.editdata(message.from_user.id,"head",data)
    app.send_message(message.chat.id, f"__🔼 Header is set to: **{data}**\nTo remove Header use **/head**__", reply_to_message_id=message.id)


# foot
@app.on_message(filters.command(["foot"]))
def foot(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    try:
        data = message.text.split("/foot ")[1]
    except:
        if backend.getdata(message.from_user.id,"foot") != "":
            backend.editdata(message.from_user.id,"foot","")
            app.send_message(message.chat.id, "__🔽 Footer is Removed__", reply_to_message_id=message.id)
        else:
            app.send_message(message.chat.id, "__🔻 Footer is not Set,\nTo set Footer use **/foot text**__", reply_to_message_id=message.id)
        return

    backend.editdata(message.from_user.id,"foot",data)
    app.send_message(message.chat.id, f"__🔽 Footer is set to: **{data}**\nTo remove Footer use **/foot**__", reply_to_message_id=message.id)


# text
@app.on_message(filters.command(["onlylinks"]))
def other(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    if backend.getdata(message.from_user.id,"text"):
        backend.editdata(message.from_user.id,"text",0)
        app.send_message(message.chat.id, "__💠 Other Text/s will be Removed\nuse **/onlylinks** to change__", reply_to_message_id=message.id)
    else:
        backend.editdata(message.from_user.id,"text",1)
        app.send_message(message.chat.id, "__💠 Other Text/s will be not be Removed\nuse **/onlylinks** to change__", reply_to_message_id=message.id)


# image
@app.on_message(filters.command(["noimage"]))
def image(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    if backend.getdata(message.from_user.id,"image"):
        backend.editdata(message.from_user.id,"image",0)
        app.send_message(message.chat.id, "__💠 Image/s will be Removed\nuse **/noimage** to change__", reply_to_message_id=message.id)
    else:
        backend.editdata(message.from_user.id,"image",1)
        app.send_message(message.chat.id, "__💠 Image/s will be not be Removed\nuse **/noimage** to change__", reply_to_message_id=message.id)


async def paarselinks(message,messagetext):

    if "https://dood" in messagetext:
        if manage.db.is_user_with_dood_key(message.from_user.id):
            msg = await app.send_message(message.chat.id, "__🔄 Processing__", reply_to_message_id=message.id)
            key = manage.db.fetch_dood_key(message.from_user.id)[0]
            link = ""
            for ele in messagetext.split("\n"):
                if "https://dood" in ele:
                    link = link + backend.copyvideo(key,ele) + "\n"
                else:
                    link = link + ele + "\n"
        else:
            await app.send_message(message.chat.id, "__⁉️ You have not logged in with Dood,\nuse /dood__", reply_to_message_id=message.id)
            return
    else:
        link = messagetext
        msg = None

    if SITE_URL in link:
        if msg == None:
            msg = await app.send_message(message.chat.id, "__🔄 Processing__", reply_to_message_id=message.id)
        temp = ""
        for ele in link.split("\n"):
            if SITE_URL in ele: 
                temp = temp + backend.bypass_reduxplayer(ele) + "\n"
            else:
                temp = temp + ele + "\n"
        link = temp

    if "mdisk.website" in link:
        if msg == None:
            msg = await app.send_message(message.chat.id, "__🔄 Processing__", reply_to_message_id=message.id)
        temp = ""
        for ele in link.split("\n"):
            if SITE_URL in ele: 
                ele = ele.replace("link.mdisk.website","links.mdisk.website")
                temp = temp + backend.bypassmdisk("https://links.mdisk.website",ele) + "\n"
            else:
                temp = temp + ele + "\n"
        link = temp

    if True:
        if msg == None:
            msg = await app.send_message(message.chat.id, "__🔄 Processing__", reply_to_message_id=message.id)
        token = manage.db.fetch_token(message.from_user.id)[0]
        text = ""
        tot = len(link.split("\n"))
        i = 0
        flag = backend.getdata(message.from_user.id,"text")
        for ele in link.split("\n"):
            i += 1
            if "http" in ele:
                resp = backend.request_url(token,ele)
                if resp["status"] == "success":   
                    surl = resp["shortenedUrl"]
                    text = text + f"**{surl}**\n"
                if i % 2 == 0:
                    try:
                        await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f"__🔄 Processed:  {i} / {tot}__")
                    except:
                        pass
            elif flag:
                text = text + ele + "\n"
        if text == "":
            await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text="__⛔ Invalid Link__") 
        else:
            head = backend.getdata(message.from_user.id,"head")
            if head != "":
                text = head + "\n" + text
            foot = backend.getdata(message.from_user.id,"foot")
            if foot != "":
                text = text + foot
            return text,msg


# text
import asyncio
@app.on_message(filters.text)
async def short(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):

    if message.text[0] == "/":
        await app.send_message(message.chat.id, "__⏩ see /help__", reply_to_message_id=message.id)
        return
    
    if not manage.db.is_user_with_token(message.from_user.id):
        await app.send_message(message.chat.id, "__⁉️ You have not logged in with token,\nuse /token__", reply_to_message_id=message.id)
        return

    text,msg = await paarselinks(message,message.text)
    await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=text) 
    

# photo
@app.on_message(filters.photo)
async def photo(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    if message.caption == None:
        await app.send_message(message.chat.id, "__❔ Send Image with Caption__", reply_to_message_id=message.id)
        return
    
    text,msg = await paarselinks(message,message.caption)
    if backend.getdata(message.from_user.id,"image"):
        await app.delete_messages(message.chat.id, [msg.id])
        await app.send_photo(message.chat.id,message.photo.file_id,text)
    else:
        await app.edit_message_text(message.chat.id,msg.id,text)


# upload
async def localupload(message):

    if manage.db.is_user_with_dood_key(message.from_user.id) and manage.db.is_user_with_token(message.from_user.id):
        token = manage.db.fetch_token(message.from_user.id)[0]
        key = manage.db.fetch_dood_key(message.from_user.id)[0]
        msg = await app.send_message(message.chat.id, "__⬇️ Downloading__", reply_to_message_id=message.id)
        file = await app.download_media(message)

        await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text="__⬆️ Uploading__")
        d = DoodStream(key)
        try:
            info = d.local_upload(file)
        except Exception as e:
            await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f"__⛔ Error : {e}__")
            return

        print(info)

        if info["msg"] == "OK":
            await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text="__🔄 Processing__")
            link = "https://doodstream.com/d/" + info["result"][0]["filecode"]
            resp = backend.request_url(token,link)
            if resp["status"] == "success": 
                surl = resp["shortenedUrl"]
                await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f'**{surl}**')
            else:
                await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f'__Failed as {resp["message"]}__') 
        else:
            await app.edit_message_text(chat_id=message.chat.id, message_id=msg.id, text=f'__Failed as {info["msg"]}__') 
        os.remove(file)
    else:
        await app.send_message(message.chat.id, "__⁉️ You either not logged in with Token or Dood Key.\nuse /token  or  use /dood__", reply_to_message_id=message.id)


# video
@app.on_message(filters.video)
async def video(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    return await localupload(message)


# document
@app.on_message(filters.document)
async def document(client: pyrogram.client.Client, message: pyrogram.types.messages_and_media.message.Message):
    if message.document.file_name.split(".")[-1] in ["mp4","mkv","wmv", "avi", "mpeg4", "mpegps", "flv", "3gp", "webm", "mov", "mpg", "m4v"]:
        return await localupload(message)
    else:
        await app.send_message(message.chat.id, f"__‼️ Not Supported Extension : **{message.document.file_name.split('.')[-1]}** \nSupported Extensions : **mkv, mp4, wmv, avi, mpeg4, mpegps, flv, 3gp, webm, mov, mpg & m4v**__", reply_to_message_id=message.id)


# server loop
print("🟢 Bot Started")
app.run()