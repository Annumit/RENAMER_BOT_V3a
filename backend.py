#import requests
from lxml import html
import json
import matplotlib.pyplot as plt
import numpy as np
from bs4 import BeautifulSoup
import sqlite3
import time
import bs4
import cloudscraper
import json

SITE_URL = "reduxplay.com"

def bypass_reduxplayer(url):

    requests = cloudscraper.create_scraper()
    res = requests.get(url)
    soup = BeautifulSoup(res.text,"html.parser")

    soup = soup.find("form")
    action = "https://reduxplay.com" + soup.get("action")
    soup = soup.findAll("input")
    data = {
        '_method': soup[0].get("value"),
        '_csrfToken': soup[1].get("value"),
        'ad_form_data': soup[2].get("value"),
        '_Token[fields]': soup[3].get("value"),
        '_Token[unlocked]': soup[4].get("value"),
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'en-US,en;q=0.5',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'Origin': 'https://reduxplay.com',
        'Alt-Used': 'reduxplay.com',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',

    }

    time.sleep(2)
    response = requests.post(action, headers=headers, data=data).json()
    if response["status"] == "success":
        return response["url"]
    else:
        return url


def bypassmdisk(domain, url):

    sess = cloudscraper.create_scraper()
    res = sess.get(url)
    soup = BeautifulSoup(res.text,"html.parser")
    soup = soup.find("form").findAll("input")
    datalist = []
    for ele in soup:
        datalist.append(ele.get("value"))

    data = {
            '_method': datalist[0],
            '_csrfToken': datalist[1],
            'ad_form_data': datalist[2],
            '_Token[fields]': datalist[3],
            '_Token[unlocked]': datalist[4],
        }

    sess.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'en-US,en;q=0.5',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest',
            'Origin': domain,
            'Connection': 'keep-alive',
            'Referer': url,
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            }

    print("waiting 5 secs")
    time.sleep(5) # important
    response = sess.post(domain+'/links/go', data=data).json()
    furl = response["url"]
    print(furl)
    return furl


def login_init():

    requests = cloudscraper.create_scraper()
    res = requests.get(f"https://{SITE_URL}/auth/signin")
    temp = html.fromstring(res.text)

    csrf_token = temp.xpath("/html/body/div[1]/div[2]/form/div[1]/input[2]")[0].value
    Token_fields = temp.xpath("/html/body/div[1]/div[2]/form/div[5]/input[1]")[0].value
    AppSession = str(res.headers["Set-Cookie"])[11:]
    AppSession = AppSession[: AppSession.find(";")]

    return [csrf_token, Token_fields, AppSession]


def login(username, password):

    init_data = login_init()

    headersList = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Content-Type": "application/x-www-form-urlencoded",
        "Origin": f"https://{SITE_URL}",
        "Connection": "keep-alive",
        "Referer": f"https://{SITE_URL}/auth/signin",
        "Cookie": f"cookieLaw=got_it; AppSession={init_data[2]}; csrfToken={init_data[0]}; ab=2",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-User": "?1",
        "TE": "trailers",
    }

    payload = f"_method=POST&_csrfToken={init_data[0]}&username={username}&password={password}&remember_me=1&_Token%5Bfields%5D={init_data[1]}&_Token%5Bunlocked%5D=adcopy_challenge%257Cadcopy_response%257Ccaptcha_code%257Ccaptcha_namespace%257Cg-recaptcha-response"
    requests = cloudscraper.create_scraper()
    response = requests.post(
        f"https://{SITE_URL}/auth/signin",
        data=payload,
        headers=headersList,
        allow_redirects=False,
    )

    if response.status_code == 200:
        return 0

    AppSession = response.cookies.get_dict()["AppSession"]
    RememberMe = response.cookies.get_dict()["RememberMe"]

    for cookie in response.cookies:
        if cookie.name == "RememberMe":
            RememberMe_expires = cookie.expires

    return [AppSession, RememberMe, init_data[0], RememberMe_expires, init_data[1]]


def dashboard(AppSession, RememberMe, csrfToken):

    headersList = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Referer": "https://{SITE_URL}/auth/signin",
        "Connection": "keep-alive",
        "Cookie": f"AppSession={AppSession}; csrfToken={csrfToken}; ab=2; RememberMe={RememberMe}",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-User": "?1",
        "TE": "trailers",
    }

    requests = cloudscraper.create_scraper()
    response = requests.get(
        f"https://{SITE_URL}/member/dashboard", headers=headersList
    )
    return response


def cut(string):
    if string == "$0.000000":
        return "$0.0000"

    if string == "0":
        return "$0.0"

    while string[-1] == "0":
        string = string[:-1]

    return string


class user_data:
    def __init__(self, response) -> None:
        self.res = response
        self.source = html.fromstring(response.text)
        self.total_view = self.source.xpath(
            "/html/body/div[1]/div[1]/section[2]/div[3]/div[1]/div/div[1]/h3" 
        )[0].text_content()
        self.total_earning = self.source.xpath(
            "/html/body/div[1]/div[1]/section[2]/div[3]/div[2]/div/div[1]/h3"
        )[0].text_content()
        self.total_refferal_earning = self.source.xpath(
            "/html/body/div[1]/div[1]/section[2]/div[3]/div[3]/div/div[1]/h3"
        )[0].text_content()
        self.avg_cpm = self.source.xpath(
            "/html/body/div[1]/div[1]/section[2]/div[3]/div[4]/div/div[1]/h3"
        )[0].text_content()[1:]
        self.avg_cpm = self.avg_cpm.replace("\n", "").replace(" ", "")
        soup = BeautifulSoup(response.text, "lxml")
        self.table = (
            ""  # "Date\t      Views 	Link Earnings 	Daily CPM Referral Earnings"
        )
        for i in soup.table.find_all("tr")[1:]:
            j = i.find_all("td")
            g = str(j[3].text)
            g = g.replace("\n", "").replace(" ", "")
            self.table += "%-8s%-10s%-11s%-8s%-10s\n" % (
                j[0].text[5:],
                j[1].text,
                cut(j[2].text),
                cut(g),
                cut(j[4].text),
            )

    def stats(self, path="img.png"):

        stats_temp = BeautifulSoup(self.res.text, "lxml")
        stats_temp: bs4.element.Tag = stats_temp.find_all("script")[6]
        stats_temp = str(stats_temp.text)[157:-261].split("},")

        xpoints = np.array([], dtype=str)
        ypoints = np.array([], dtype=int)
        for i in stats_temp:
            temp = json.loads(
                (i + "}").replace("date", '"date"').replace("views", '"views"')
            )
            xpoints = np.append(xpoints, temp["date"][8:])
            ypoints = np.append(ypoints, temp["views"])

        fig, ax = plt.subplots()

        ax.set_xlabel("day")
        ax.set_ylabel("views")
        ax.set_title("Statistics")
        ax.tick_params(axis="x", colors="blue")
        ax.tick_params(axis="y", colors="green")

        # plotting a line plot after changing it's width and height
        f = plt.figure()
        f.set_figwidth(10)
        f.set_figheight(4)

        plt.ylim([0, max(ypoints)])
        plt.yticks(range(0, max(ypoints) + 1, 1))

        plt.xlim([0, len(xpoints)])
        plt.plot(xpoints, ypoints)
        plt.savefig(path, bbox_inches="tight", pad_inches=0.2)


class database:
    def __init__(self, database_path="tokens.db"):
        self.con = sqlite3.connect(database_path, check_same_thread=False)
        self.cur = self.con.cursor()
        self.cur.execute(
            """CREATE TABLE IF NOT EXISTS tokens ( 
                            telegram_user_id int unique , 
                            token TEXT , 
                            RememberMe TEXT,
                            RememberMe_expires TEXT ,
                            username TEXT,
                            password TEXT,
                            dood_key TEXT
                        )"""
        )
        self.con.commit()

    def fetch_dood_key(self, telegram_user_id):
        self.cur.execute(
            "select dood_key from tokens where telegram_user_id = ?",
            (telegram_user_id,),
        )
        rv = self.cur.fetchone()
        self.con.commit()
        return rv

    def is_user_with_dood_key(self, telegram_user_id):
        rv = self.fetch_dood_key(telegram_user_id)
        if rv == None or rv == (None,):
            return 0
        return 1

    def update_dood_key(self, telegram_user_id, dood_key):
        self.cur.execute(
            """UPDATE tokens   SET dood_key = ?  WHERE telegram_user_id = ? """,
            (dood_key, telegram_user_id),
        )
        self.con.commit()

    def add_with_dood_key(self, telegram_user_id, dood_key):
        if (self.is_user_with_username(telegram_user_id) or self.is_user_with_token( telegram_user_id )):
            self.update_dood_key(telegram_user_id,dood_key)
        else:
            self.cur.execute(
                """INSERT INTO 
                            tokens ( telegram_user_id ,dood_key ) 
                            VALUES (?,?)""",
                (telegram_user_id, dood_key),
            )
        self.con.commit()

    def delete_dood_key(self, telegram_user_id):
        self.update_dood_key(telegram_user_id, None)
        self.con.commit()

    def __del__(self):
        self.con.commit()
        self.con.close()

    def fetch_token(self, telegram_user_id):
        self.cur.execute(
            "select token from tokens where telegram_user_id = ?", (telegram_user_id,)
        )
        rv = self.cur.fetchone()
        self.con.commit()
        return rv

    def fetch_username(self, telegram_user_id):
        self.cur.execute(
            "select username from tokens where telegram_user_id = ?",
            (telegram_user_id,),
        )
        rv = self.cur.fetchone()
        self.con.commit()
        return rv

    def fetch_all(self, telegram_user_id):
        self.cur.execute(
            "select * from tokens where telegram_user_id = ?", (telegram_user_id,)
        )
        rv = self.cur.fetchone()
        self.con.commit()
        return rv

    def is_user_with_token(self, telegram_user_id):
        rv = self.fetch_token(telegram_user_id)
        if rv == None or rv == (None,):
            return 0
        return 1

    def is_user_with_username(self, telegram_user_id):
        rv = self.fetch_username(telegram_user_id)
        if rv == None or rv == (None,):
            return 0
        return 1

    def add_with_username(
        self, telegram_user_id, RememberMe, RememberMe_expires, username, password
    ):

        self.cur.execute(
            """INSERT INTO 
                            tokens ( telegram_user_id , RememberMe ,RememberMe_expires ,username ,password ) 
                            VALUES (?,?,?,?,?)""",
            (telegram_user_id, RememberMe, RememberMe_expires, username, password),
        )
        self.con.commit()

    def add_with_token(self, telegram_user_id, token):
        self.cur.execute(
            """INSERT INTO 
                            tokens ( telegram_user_id , token ) 
                            VALUES (?,?)""",
            (telegram_user_id, token),
        )
        self.con.commit()

    def delete_user(self, telegram_user_id):
        self.cur.execute(
            """DELETE from 
                            tokens where telegram_user_id = ?""",
            (telegram_user_id,),
        )
        self.con.commit()

    def update_re(self, telegram_user_id, RememberMe, RememberMe_expires):
        self.cur.execute(
            """UPDATE tokens 
                            SET RememberMe = ? ,
                                RememberMe_expires= ?
                            WHERE telegram_user_id = ? """,
            (RememberMe, RememberMe_expires, telegram_user_id),
        )
        self.con.commit()

    def update_token(self, telegram_user_id, token):
        self.cur.execute(
            """UPDATE tokens 
                            SET token = ? 
                            WHERE telegram_user_id = ? """,
            (token, telegram_user_id),
        )
        self.con.commit()

    def update_username(
        self, telegram_user_id, RememberMe, RememberMe_expires, username, password
    ):
        self.cur.execute(
            """UPDATE tokens 
                            SET RememberMe = ? ,
                                RememberMe_expires= ?,
                                username = ?,
                                password = ?
                            WHERE telegram_user_id = ? """,
            (RememberMe, RememberMe_expires, username, password, telegram_user_id),
        )
        self.con.commit()


class Manager:
    def __init__(self):
        self.db = database()

    def add_user_with_username(self, telegram_user_id, username, password):
        rv = login(username, password)
        if rv == 0:
            return 0

        if self.db.is_user_with_username(telegram_user_id):
            self.db.update_re(telegram_user_id, rv[1], rv[3])
        elif self.db.is_user_with_token(telegram_user_id):
            self.db.update_username(telegram_user_id, rv[1], rv[3], username, password)
        else:
            self.db.add_with_username(
                telegram_user_id, rv[1], rv[3], username, password
            )
        return 1

    def add_user_with_token(self, telegram_user_id, token):
        if self.db.is_user_with_username(telegram_user_id):
            self.db.update_token(telegram_user_id, token)
        else:
            self.db.add_with_token(telegram_user_id, token)

    def scrape(self, telegram_user_id) -> user_data:

        if not self.db.is_user_with_username(telegram_user_id):
            return 0

        rv = self.db.fetch_all(telegram_user_id)

        if rv == (None,) or rv == None:
            return 0

        if int(rv[3]) <= int(time.time()) or 1:
            print("login again")
            self.add_user_with_username(rv[0], rv[4], rv[5])

        csrf_token, Token_fields, AppSession = login_init()
        rs = dashboard(AppSession, rv[2], csrf_token)
        return user_data(rs)

    def delete_user(self, telegram_user_id):
        return self.db.delete_user(telegram_user_id)

    def update_token(self, telegram_user_id, token):
        self.db.update_token(telegram_user_id, token)

    def create_url(self, token, url, alias=""):
        requests = cloudscraper.create_scraper()
        if self.db.is_user_with_token(token):
            return requests.get(
                f"https://{SITE_URL}/api?api={token}&url={url}&alias={alias}"
            ).json()
        return 0


def request_url(token, url):
    requests = cloudscraper.create_scraper()
    resp = requests.get(f"https://{SITE_URL}//api?api={token}&url={url}").json()
    print(resp)
    return resp


##############################################################################

from doodstream import DoodStream

def checkkey(key):
    d = DoodStream(key)
    if d.account_info()["msg"] == "OK":
        return 1
    return 0

def checkfileid(key,link):
    d = DoodStream(key)
    fileid = link.split("/")[-1]
    if d.file_info(fileid)["msg"] == "OK":
        return 1
    return 0

def copyvideo(key,link):
    d = DoodStream(key)
    fileid = link.split("/")[-1]
    if checkfileid(key,fileid):
        cfileid = d.copy_video(fileid)
        print(cfileid)
        cfileid = cfileid["result"]["filecode"]
        return "https://doodstream.com/d/" + cfileid
    return link


###################################################################

class userdata:
    userid: str
    header: str
    footer: str
    other: int
    image: int

datalist = []

def getuser(id):
    for ele in datalist:
        if ele.userid == id:
            return ele

    use = userdata()
    use.userid = id
    use.header = ""
    use.footer = ""
    use.other = 1
    use.image = 1
    datalist.append(use)
    return use

def editdata(id,mod,val):
    use = getuser(id)
    if mod == "head":
        use.header = val
    elif mod == "foot":
        use.footer = val
    elif mod == "text":
        use.other = val
    elif mod == "image":
        use.image = val
    return use

def getdata(id,mod):
    use = getuser(id)
    if mod == "head":
        return use.header
    elif mod == "foot":
        return use.footer
    elif mod == "text":
        return use.other
    elif mod == "image":
        return use.image
    